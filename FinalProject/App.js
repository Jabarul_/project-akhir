import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ScroolView } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';

import SplashScreen from './components/ShopApp/SplashScreen';
import WelcomeScreen from './components/ShopApp/WelcomeScreen';
import LoginScreen from './components/ShopApp/LoginScreen';
import RegisterScreen from './components/ShopApp/RegisterScreen';
import HomeScreen from './components/ShopApp/HomeScreen';
import CartScreen from './components/ShopApp/CartScreen';
import ChatScreen from './components/ShopApp/ChatScreen';
import ProfileScreen from './components/ShopApp/ProfileScreen';

const RootStack = createStackNavigator();
const TabsStack = createBottomTabNavigator();

const TabsStackScreen = () => (
    <TabsStack.Navigator initialRouteName='Home' tabBarOptions={{ activeTintColor: '#820303' }}>
        <TabsStack.Screen name='Home' component={HomeScreen}
            options={{
                headerShown: false,
                tabBarLabel: 'Home',
                tabBarIcon: ({ color, size }) => (
                    <Icon name="home" color={color} size={size} />
                ),
            }} 
        />
        <TabsStack.Screen name='Cart' component={CartScreen}
            options={{
                headerShown: false,
                tabBarLabel: 'Cart',
                tabBarIcon: ({ color, size }) => {
                    <Icon name="shopping-cart" color={color} size={2} />
                },
            }} 
        />
        <TabsStack.Screen name='Chat' component={ChatScreen}
            options={{
                headerShown: false,
                tabBarLabel: 'Chat',
                tabBarIcon: ({ color, size }) => {
                    <Icon name="chat-bubble" color={color} size={size} />
                }, 
            }}
        />
        <TabsStack.Screen name='Profile' component={ProfileScreen}
            options={{
                headerShown: false,
                tabBarLabel: 'Profile',
                tabBarIcon: ({ color, size }) => {
                    <Icon name="account-circle" color={color} size={size} />
                },
            }}
        />
    </TabsStack.Navigator>
);

const RootStackScreen = () => (
    <RootStack.Navigator>
        <RootStack.Screen name='SplashScreen' component={SplashScreen} options={{headerShown: false}} />
        <RootStack.Screen name='Welcome' component={WelcomeScreen} options={{headerShown: false}} />
        <RootStack.Screen name='Signin' component={LoginScreen} options={{headerShown: false}} />
        <RootStack.Screen name='Signup' component={RegisterScreen} options={{headerShown: false}} />
        <RootStack.Screen name='Tabsstack' component={TabsStackScreen} options={{headerShown: false}} />
    </RootStack.Navigator>
);

export default () => (
    <NavigationContainer>
        <RootStackScreen />
    </NavigationContainer>
);