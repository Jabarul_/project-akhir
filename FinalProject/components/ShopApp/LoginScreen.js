import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, ImageBackground } from 'react-native'

const LoginScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <ImageBackground source={require('../../assets/background.png')} style={styles.background}>
                <Image source={require('../../assets/logobird.png')} style={styles.logologin} />
                
                <Text style={styles.tekslogin}>Welcome Back!</Text>         

                <View style={{alignItems: 'center', marginTop: 40}}>
                    <TextInput style={styles.input} placeholder={'email'}/>
                </View>

                <View style={{alignItems: 'center', marginTop: 20}}>
                    <TextInput style={styles.input} secureTextEntry={true} placeholder={'password'}/>
                </View>

                <View style={{ marginHorizontal: 68, marginTop: 55, flexDirection: 'row' }}>
                    <TouchableOpacity style={styles.btnback} onPress={ () => navigation.goBack()} >
                        <Text style={styles.btnbackteks}>Back</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnlog} onPress={ () => navigation.navigate('Tabsstack')} >
                        <Text style={styles.btnlogteks}>Sign In</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.account}>
                    <Text style={styles.ask}>Don't have account ?</Text>
                    <TouchableOpacity onPress={ () => navigation.navigate('Signup') }>
                        <Text style={styles.sign}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 1,
        resizeMode: 'cover'
    },
    logologin: {
        width: 261.84,
        height: 108.7,
        marginTop: 60,
        marginHorizontal: 49
    },
    tekslogin: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#820303',
        marginLeft: 52,
        marginTop: 50
    },
    input: {
        width: 250,
        height: 40,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 1,
        borderColor: '#000',
        alignItems: 'center',
        paddingLeft: 8
    },
    btnback: {
        padding: 10,
        backgroundColor: '#fff',
        borderColor: '#820303',
        borderWidth: 1,
        borderRadius: 5,
        // shadowColor: '#820303',
        // shadowOpacity: 1,
        // elevation: 3,
        // shadowOffset: {width: 2, height: 2},
        // shadowRadius: 3,
        width: 98,
        height: 40,
        marginRight: 30
    },
    btnbackteks: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#820303',
        lineHeight: 20
    },
    btnlog: {
        padding: 10,
        backgroundColor: '#820303',
        borderRadius: 5,
        width: 98,
        height: 40,
        marginRight: 30
    },
    btnlogteks: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#fff',
        lineHeight: 20
    },
    account: {
        flexDirection: 'row',
        width: 182,
        height: 20,
        marginTop: 34,
        marginHorizontal: 74
    },
    ask: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#000'
    },
    sign: {
        marginLeft: 12,
        fontSize: 15,
        color: '#e40202',
        fontWeight: 'bold'
    }
})
