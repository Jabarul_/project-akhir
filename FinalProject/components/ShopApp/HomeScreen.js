import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TextInput, ScrollView, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

import LukisanScreen from '../ShopApp/LukisanScreen';
import Icon from 'react-native-vector-icons/MaterialIcons';


const HomeScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            
            <View style={{backgroundColor: '#FFFFFF', height: 80, width: 360, borderBottomWidth: 1, borderBottomColor: 'rgba(0, 0, 0, 0.3)'}}>

                <View style={{flexDirection: 'row', marginVertical: 20}}>

                    <View style={{flexDirection: 'row', paddingTop: 10, paddingBottom: 10, padingRight: 10, paddingLeft: 4}}>
                        
                        <TextInput style={styles.inputsearch} placeholder = "Search" />
                        <Icon name={'search'} style={styles.iconsearch} size={30} color={'#820303'} />
                        
                    </View>

                    <View style={{flexDirection: 'row', marginLeft: 200}}>
                        <Icon name={'favorite'} style={styles.iconpendukung} size={30} />
                        <Icon name={'redeem'} style={styles.iconpendukung} size={30} />
                        <Icon name={'more-vert'} style={styles.iconpendukung} size={30} />
                    </View>
                    
                </View>

            </View>
            <ScrollView>
                <View style={{alignItems: 'center'}}>
                    <Image source={require('../../assets/taripapua.png')} style={styles.fotohome}/>
                </View>
                <Text style={styles.headfresh}>Lukisan</Text>
            
                <LukisanScreen />
            </ScrollView>
            
        </View>
    )
}


export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    iconsearch: {
        marginVertical: 6,
        marginLeft: -225,
    },
    inputsearch: {
        width: 240,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#820303',
        paddingLeft: 20,
        paddingRight: 10,
        marginBottom: 10,
        marginLeft: 10,
        backgroundColor: '#F3F3F3',
        paddingLeft: 50
    },
    iconpendukung: {
        marginRight: 4,
        marginVertical: 15,
        color: 'rgba(0, 0, 0, 0.6)'
    },
    fotohome: {
        width: 338,
        height: 195,
        borderRadius: 5,
        marginTop: 10
    },
    list1: {
        backgroundColor: '#FFFFFF',
        height: 110,
        width: 75,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#EAEAEA',
        marginRight: 14,
        marginLeft: 13,
    },
    list: {
        backgroundColor: '#FFFFFF',
        height: 110,
        width: 75,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#EAEAEA',
        marginRight: 14
    },
    fotoicon: {
        marginTop: 5,
        marginLeft: 7,
        width: 60,
        height: 60,
        marginBottom: 10
    },
    teksicon: {
        color: '#000000',
        fontSize: 12,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    headfresh: {
        marginLeft: 11,
        fontWeight: 'bold',
        color: '#820303',
        fontSize: 24,
        marginTop: 18,
        marginBottom: 5
    },
    headbuah: {
        marginLeft: 11,
        fontWeight: 'bold',
        color: '#820303',
        fontSize: 24,
        marginTop: 18,
    },
    datafruit: {
        width: 120,
        height: 150,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        marginRight: 10,
        marginLeft: 12,
        flexDirection: 'row',
        marginBottom: 10
        
    },
    headvege: {
        marginLeft: 12,
        fontWeight: 'bold',
        color: '#139038',
        fontSize: 20,
        marginTop: 15,
        marginBottom: 5
    },
    datavege: {
        width: 120,
        height: 150,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        marginRight: 10,
        marginLeft: 12
        
    },
})