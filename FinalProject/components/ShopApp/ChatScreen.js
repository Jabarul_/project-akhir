import React from 'react';
import { StyleSheet, Text, View, ScroolView } from 'react-native';

const ChatScreen = () => {
    return(
        <View style={styles.container}>
            <Text style={styles.teks}>Tampilan Default</Text>
        </View>
    )
}

export default ChatScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    teks: {
        textAlign: 'center',
    }
})