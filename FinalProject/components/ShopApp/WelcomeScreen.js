import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground } from 'react-native'

const WelcomeScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <ImageBackground source={require('../../assets/background.png')} style={styles.background}>
                <Image source={require('../../assets/logotifa.png')} style={styles.logo} />

                <View style={styles.tekswel}>
                    <Text style={styles.tekswelcome}>Welcome</Text>
                </View>

                <View style={styles.teksjudul}>
                    <Text style={styles.teks}>Bangga dengan karya anak bangsa !</Text>
                    <Text style={styles.teks} marginTop={5}>Bangga dengan karya anak timur !</Text>
                </View>

                <View style={styles.btn}>
                    <TouchableOpacity style={styles.btnlog} onPress={ () => navigation.navigate('Signin') }>
                        <Text style={styles.btnlogteks}>Sign In</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnreg} onPress={ () => navigation.navigate('Signup') }>
                        <Text style={styles.btnregteks}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </View>
    )
}

export default WelcomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 1,
        resizeMode: 'cover'
    },
    logo: {
        width: 213,
        height: 264,
        marginHorizontal: 72,
        marginTop: 30
    },
    tekswel: {
        marginTop: 20
    },
    tekswelcome: {
        fontWeight: 'bold',
        fontSize: 36,
        color: '#820303',
        textAlign: 'center'
    },
    teksjudul: {
        marginTop: 20,
    },
    teks: {
        fontWeight: 'bold',
        fontSize: 14,
        color: '#000000',
        textAlign: 'center'
    },
    btn: {
        marginTop: 40,
        flexDirection: 'column',
        alignItems: 'center'
    },
    btnlog: {
        padding: 10,
        backgroundColor: '#820303',
        borderRadius: 15,
        width: 228,
        height: 38,
        marginBottom: 30
    },
    btnlogteks: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 18
    },
    btnreg: {
        padding: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 15,
        width: 228,
        height: 38,
        borderColor: '#820303',
        borderWidth: 1
    },
    btnregteks: {
        textAlign: 'center',
        color: '#820303',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 18
    }
})