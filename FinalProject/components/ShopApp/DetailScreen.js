import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Detail extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={{backgroundColor: '#FFFFFF', height: 80, width: 360, borderBottomWidth: 1, borderBottomColor: 'rgba(0, 0, 0, 0.3)'}}>

                    <View style={{flexDirection: 'row', marginVertical: 20}}>

                        <View style={{flexDirection: 'row', paddingTop: 10, paddingBottom: 10, padingRight: 10, paddingLeft: 4}}>
                            <TouchableOpacity onPress={() => navigation.goBack()}>
                                <Icon name={'arrow-back'} style={styles.iconback} size={30} color={'#820303'} />
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection: 'row', marginLeft: 200}}>
                            <Icon name={'shopping-cart'} style={styles.iconpendukung} size={30} />
                            <Icon name={'more-vert'} style={styles.iconpendukung} size={30} />
                        </View>

                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    iconback: {
        lineHeight: 20
    }
})