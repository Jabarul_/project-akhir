import React, { useEffect } from 'react';
import { StyleSheet, Text, View, ScroolView, Button, Image } from 'react-native';

const Splash = ({ navigation }) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace('Welcome');
        }, 3000)
    }, []);

    return(
        <View style={styles.container}>
            <Image style={styles.logo} source={require('../../assets/SplashLogo.png')}/>
        </View>
    )
}

export default Splash;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 145,
        height: 356
    }
})