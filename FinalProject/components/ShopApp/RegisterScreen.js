import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, ImageBackground } from 'react-native'

const RegisterScreen = ({ navigation }) => {
    return(
        <View style={styles.container}>
            <ImageBackground source={require('../../assets/background.png')} style={styles.background}>
            <View style={styles.sub}>    
                <Text style={styles.tekssignup}>Let's Start</Text>

                <View style={styles.formusername}>
                    <Text style={styles.teksform}>Nama Lengkap</Text>
                    <TextInput style={styles.input}/>
                </View>

                <View>
                    <Text style={styles.teksform}>Username</Text>
                    <TextInput style={styles.input}/>
                </View>

                <View>
                    <Text style={styles.teksform}>Email</Text>
                    <TextInput style={styles.input}/>
                </View>

                <View>
                    <Text style={styles.teksform}>Password</Text>
                    <TextInput style={styles.input}/>
                </View>
                
                <View>
                    <Text style={styles.teksform}>Ulang Password</Text>
                    <TextInput style={styles.input}/>
                </View>

                <View style={{ marginHorizontal: 68, marginTop: 38, flexDirection: 'row' }}>
                    <TouchableOpacity style={styles.btnback} onPress={ () => navigation.goBack()} >
                        <Text style={styles.btnbackteks}>Back</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnlog} onPress={ () => navigation.navigate('Signin')} >
                        <Text style={styles.btnlogteks}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ImageBackground>
        </View>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    sub: {
        alignItems: 'center'
    },
    background: {
        flex: 1,
        resizeMode: 'cover'
    },
    tekssignup: {
        fontSize:28,
        fontWeight: 'bold',
        color: '#820303',
        textAlign: 'center',
        marginTop: 70
    },
    formusername: {
        marginTop: 42,
    },
    teksform: {
        color: '#000',
        fontSize: 14,
        fontWeight: 'bold',
        marginBottom: 10
    },
    input: {
        width: 241,
        height: 36,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.5)',
        marginBottom: 10,
        padding: 10
    },
    btnlog: {
        padding: 10,
        backgroundColor: '#820303',
        borderRadius: 5,
        width: 116,
        height: 35,
    },
    btnlogteks: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold',
        lineHeight: 15
    },
    btnback: {
        padding: 10,
        backgroundColor: '#fff',
        borderColor: '#820303',
        borderWidth: 1,
        borderRadius: 5,
        width: 116,
        height: 35,
        marginRight: 9
    },
    btnbackteks: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#820303',
        lineHeight: 16
    },
});