import React from 'react';
import { StyleSheet, Text, View, ScroolView } from 'react-native';

const ProfileScreen = () => {
    return(
        <View style={styles.container}>
            <Text style={styles.teks}>Tampilan Default</Text>
        </View>
    )
}

export default ProfileScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    teks: {
        textAlign: 'center',
    }
})