import React from 'react';
import { StyleSheet, Text, View, ScroolView, Button } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Splash from './SplashScreen';
import Welcome from './WelcomeScreen';
import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import HomeScreen from './HomeScreen';
import AboutScreen from './AboutScreen';
import DetailScreen from './DetailScreen';

const RootStack = createStackNavigator();
const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();

const RootStackScreen = () => {
    <RootStack.Navigator>
        <RootStack.Screen name='Splash' component={Splash} options={{headerShown: false}} />
        <RootStack.Screen name='Welcome' component={Welcome} options={{headerShown: false}} />
    </RootStack.Navigator>
}

export default () => (
    <NavigationContainer>
        <RootStackScreen />
    </NavigationContainer>
);