import React from 'react';
import { View, Image, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native';
import Axios from 'axios';

export default class LukisanScreen extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isLoading: true,
            isError: false
        };
      }
    
    componentDidMount() {
        this.getGithubUser()
    }

    getGithubUser = async () => {
        try {
            const response = await Axios.get(`https://datashop-f76a5.firebaseio.com/lukisan.json`)
            this.setState({ isError: false, isLoading: false, data: response.data })
        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }

    render(){
        return(
            <FlatList
                data={this.state.data}
                renderItem={({ item }) =>
                    <View style={{flex: 1, marginTop: 8}}>
                        
                        <View style={styles.card}>
                            <View style={{alignItems: 'center'}}>
                                <Image source={{ uri: `${item.foto}` }} style={styles.foto} />
                            </View>
                            <View style={{width: 298, height : 140, marginLeft: 8}}>
                                <View>
                                    <Text style={styles.nama}>{item.nama}</Text>
                                </View>
                                <View>
                                    <Text style={styles.harga}>{item.harga}</Text> 
                                </View>
                            </View>
                        </View>
                    </View>
                }
                keyExtractor={({ id }, index) => index}
            />
        )
    }
} 

const styles = StyleSheet.create({
    card: {
        flexDirection: 'column',
        marginBottom: 10,
        backgroundColor: "#FFFFFF",
        padding: 15,
        borderRadius: 10,
        width: 335,
        height: 261,
        marginLeft: 13,
        borderWidth: 1
    },
    nama: {
        color: '#820303',
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 12,
    },
    harga: {
        color: '#820303',
        fontSize: 12,
        fontWeight: "bold",
        marginTop: 12
    },
    foto: {
        width: 298,
        height: 175,
        borderRadius: 10
    }
})